"use strict";
exports.__esModule = true;
var express_1 = require("express");
var OrdersController_1 = require("../controllers/OrdersController");
var checkJwt_1 = require("../middlewares/checkJwt");
var checkRole_1 = require("../middlewares/checkRole");
var router = express_1.Router();
//Get all Orderss
router.get("/", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OrdersController_1["default"].listAll);
// Get one Orders
router.get("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OrdersController_1["default"].getOneById);
//getMyOrder
router.get("/getMyOrder/", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "customer", "cashier"])], OrdersController_1["default"].getMyOrder);
//getMyOrderByStatus
router.get("/getMyOrderByStatus/:status", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "customer", "cashier"])], OrdersController_1["default"].getMyOrderByStatus);
//getOrderByStatus
router.get("/:status", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "cashier"])], OrdersController_1["default"].getOrderByStatus);
//getUnverifiedOrder
router.get("/getUnverifiedOrder/", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "cashier"])], OrdersController_1["default"].getUnverifiedOrder);
//verifyOrder
router.post("/verifyOrder/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "cashier"])], OrdersController_1["default"].verifyOrder);
/*
//putOnQueueOrder
router.get(
  "/putOnQueueOrder/:id([0-9]+)",
  [checkJwt, checkRole(["admin", "chef", "cashier", "waiter"])],
  OrdersController.putOnQueueOrder
);*/
//prepareOrder
router.get("/prepareOrder/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "chef"])], OrdersController_1["default"].prepareOrder);
router.get("/fishingOrder/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "chef"])], OrdersController_1["default"].fishingOrder);
//Create a new Orders
router.post("/", [checkJwt_1.checkJwt], OrdersController_1["default"].newOrders);
//Edit one Orders
router.patch("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OrdersController_1["default"].editOrders);
//Delete one Orders
router["delete"]("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OrdersController_1["default"].deleteOrders);
exports["default"] = router;
