"use strict";
exports.__esModule = true;
var express_1 = require("express");
var OffersController_1 = require("../controllers/OffersController");
var checkJwt_1 = require("../middlewares/checkJwt");
var checkRole_1 = require("../middlewares/checkRole");
var multer = require("multer");
var moment = require("moment");
var assert = require("assert");
var router = express_1.Router();
//Get all Offerss
router.get("/", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "customer", "cashier"])], OffersController_1["default"].listAll);
// Get one Offers
router.get("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OffersController_1["default"].getOneById);
//Create a new Offers
router.post("/", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OffersController_1["default"].newOffers);
//Edit one Offers
router.patch("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OffersController_1["default"].editOffers);
//get meals By OfferlId
router.get("/getmealsByOfferlId/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "waiter", "customer", "cashier"])], OffersController_1["default"].getmealsByOfferlId);
//Delete one Offers
router["delete"]("/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin"])], OffersController_1["default"].deleteOffers);
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './src/images');
    },
    filename: function (req, file, cb) {
        cb(null, (moment(new Date())).format('DD-MMM-YYYY-HH-mm-ss-msms') + '-' + file.originalname);
    }
});
var fileFilter = function (req, file, cb) {
    if (file.mimetype === "image/jpg" ||
        file.mimetype === "image/jpeg" ||
        file.mimetype === "image/png") {
        cb(null, true);
    }
    else {
        cb(new Error("Image uploaded is not of type jpg/jpeg or png"), false);
    }
};
var upload = multer({ storage: storage, fileFilter: fileFilter });
upload; // $ExpectType Multer
assert.strictEqual(upload.constructor.name, 'Multer');
router.post('/uploadImage/:id([0-9]+)', [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "chef"])], upload.single('avatar'), OffersController_1["default"].uploadImage);
router.post("/uploadImage/:id([0-9]+)", [checkJwt_1.checkJwt, checkRole_1.checkRole(["admin", "chef"])], OffersController_1["default"].uploadImage);
exports["default"] = router;
