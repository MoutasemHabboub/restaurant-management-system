import { Router } from "express";
import OffersController from "../controllers/OffersController";
 import { checkJwt } from "../middlewares/checkJwt";
 import { checkRole } from "../middlewares/checkRole";
 import multer = require('multer');
 import * as moment from 'moment';
 import assert = require('assert');

 const router =  Router();
//Get all Offerss
router.get("/", [checkJwt, checkRole(["admin","waiter","customer","cashier"])], OffersController.listAll);

// Get one Offers
router.get("/:id([0-9]+)",[checkJwt, checkRole(["admin"])],OffersController.getOneById);

//Create a new Offers
router.post("/", [checkJwt, checkRole(["admin"])], OffersController.newOffers);

//Edit one Offers
router.patch("/:id([0-9]+)",[checkJwt, checkRole(["admin"])],OffersController.editOffers);

//get meals By OfferlId
router.get("/getmealsByOfferlId/:id([0-9]+)",[checkJwt, checkRole(["admin","waiter","customer","cashier"])],OffersController.getmealsByOfferlId);

//Delete one Offers
router.delete("/:id([0-9]+)",[checkJwt, checkRole(["admin"])],OffersController.deleteOffers);

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './src/images')
    },
    
    filename: function (req: any, file: any, cb: any) {
        cb(null, (moment(new Date())).format('DD-MMM-YYYY-HH-mm-ss-msms') + '-'+file.originalname)
    }
  });
const fileFilter = (req: any,file: any,cb: any) => {
    if(file.mimetype === "image/jpg"  || 
       file.mimetype ==="image/jpeg"  || 
       file.mimetype ===  "image/png"){
     
    cb(null, true);
   }else{
      cb(new Error("Image uploaded is not of type jpg/jpeg or png"),false);
  }
  }
  const upload = multer({storage: storage, fileFilter : fileFilter});
  
  
  upload; // $ExpectType Multer
  assert.strictEqual(upload.constructor.name, 'Multer');
  router.post('/uploadImage/:id([0-9]+)',[checkJwt, checkRole(["admin","chef"])], upload.single('avatar'),  OffersController.uploadImage);
  router.post("/uploadImage/:id([0-9]+)", [checkJwt, checkRole(["admin","chef"])], OffersController.uploadImage);
 export default router;