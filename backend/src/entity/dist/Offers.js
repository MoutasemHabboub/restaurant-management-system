"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Offers = void 0;
var typeorm_1 = require("typeorm");
var MealOffer_1 = require("./MealOffer");
var OrderOffers_1 = require("./OrderOffers");
var Offers = /** @class */ (function () {
    function Offers() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn()
    ], Offers.prototype, "id");
    __decorate([
        typeorm_1.Column()
    ], Offers.prototype, "name");
    __decorate([
        typeorm_1.Column()
    ], Offers.prototype, "price");
    __decorate([
        typeorm_1.Column({ type: 'date' })
    ], Offers.prototype, "expirationDate");
    __decorate([
        typeorm_1.Column({ "default": "http://localhost:3307/images/dish.JPG" })
    ], Offers.prototype, "imageUrl");
    __decorate([
        typeorm_1.OneToMany(function () { return MealOffer_1.MealOffer; }, function (MealOffer) { return MealOffer.Offers; }, {
            cascade: ["insert", "update"]
        })
    ], Offers.prototype, "mealOffers");
    __decorate([
        typeorm_1.OneToMany(function () { return OrderOffers_1.OrderOffers; }, function (OrderOffers) { return OrderOffers.offers; }, {
            cascade: ["insert", "update"]
        })
    ], Offers.prototype, "OrderOffers");
    Offers = __decorate([
        typeorm_1.Entity()
    ], Offers);
    return Offers;
}());
exports.Offers = Offers;
